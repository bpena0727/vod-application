// Libraries
import React from 'react';

// Components
import Menu from "./components/Menu";
import { BrowserRouter as Router } from "react-router-dom";
import Layout from "./layout";
// FontAwesome lib 
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faPlayCircle } from '@fortawesome/free-solid-svg-icons'

// Styles
import './App.css';

// declare icons to be used withn app
library.add(fab, faCheckSquare, faCoffee, faPlayCircle)

class App extends React.Component {
  render() {
    return (
      <Router>
        <section className="App">
          <Menu/>
          <div className="body">
            <Layout />
          </div>        
        </section>
      </Router>
    );
  }
}

export default App;
