const actionTypes = {
	GET_MOVIES: 'GET_MOVIES',
	OPEN_VIDEO: 'OPEN_VIDEO',
	GET_VIDEO: 'GET_VIDEO',
	ADD_HISTORY: 'ADD_HISTORY',
	GET_HISTORY: 'GET_HISTORY'
}

export default actionTypes;