export function loginAction (username, password) {
    return{
        type: 'SIGN_IN', 
        payload: {un: username, pw:password}
    }
}

export function logout () {
    return{
        type: 'SIGN_OUT'
    }
}

export function addHistory (video) {
    return{
        type: 'Add', 
        payload: video
    }
}

export function clearHistory () {
    return{
        type: 'CLEAR'
    }
}