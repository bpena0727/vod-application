import actionTypes from '../actions/actionTypes';
import API from "../middleware/api";

export function getMovies() {
	return dispatch => {
        API.get('/movies')
        .then(result => {                
            dispatch({
                type: actionTypes.GET_MOVIES,
                payload: result.data.entries
            })
        })
    }
}

export function openVideo(obj) {
	return dispatch => {
        dispatch({
            type: actionTypes.OPEN_VIDEO,
            payload: obj
        })
    }
}

export function getVideo() {
	return dispatch => {
        dispatch({
            type: actionTypes.GET_VIDEO,
        })
    }
}

export function addToHistory(obj){
    if(localStorage.getItem("history") === undefined || localStorage.getItem("history") === null){
        const historyObj = {history:[obj]}
        localStorage.setItem("history", JSON.stringify(historyObj))

        return dispatch => {
            dispatch({
                type: actionTypes.ADD_HISTORY,
                payload: historyObj
            })
        }
    }else{
        const historyObj = localStorage.getItem("history")
        const parsedJistoryObj = JSON.parse(historyObj)
        var exist = false 
        for(var i=0; i<parsedJistoryObj.history.length; i++){
            if(parsedJistoryObj.history[i].title === obj.title){
                exist = true
            }
        }
        if(exist === false){
            parsedJistoryObj.history.push(obj)
            localStorage.setItem("history", JSON.stringify(parsedJistoryObj))
        }
        return dispatch => {
            dispatch({
                type: actionTypes.ADD_HISTORY,
                payload: parsedJistoryObj
            })
        }
    }
    
}

export function getHistory() {
	return dispatch => {
        dispatch({
            type: actionTypes.GET_HISTORY,
        })
    }
}