import React from "react";

import { Route, Switch, Redirect } from "react-router-dom";
import { withRouter } from "react-router";

// Views
import Movies from "./views/Movies";
import NotFound from "./views/NotFound"
import VideoDetal from "./views/VideoDetal"
import VideoPlayer from "./views/VideoPlayer"
import History from "./views/History"
// ti be implemented
//import login from "./views/Login"

class Layout extends React.Component {

	
	render() {
		return(
			<React.Fragment>
				<Switch>
					{/* not implemented correctly
					<Route exact path="/login" component={login} /> */}
					<Route exact path="/movies" component={Movies} />
					<Route exact path="/videoDetails" component={VideoDetal} />
					<Route exact path="/videoPlayer" component={VideoPlayer} />
					<Route exact path="/history" component={History} />
					<Redirect exact from="/" to="/movies" />
					<Route path="*" component={NotFound} />
				</Switch>
			</React.Fragment>
		);
	}
}

export default withRouter(Layout);
