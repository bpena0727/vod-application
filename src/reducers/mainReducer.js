import actionTypes from "../actions/actionTypes";

const InitialState = {
	movies: [],
    movieObj: '',
    history:''
}

export const mainReducer = (state = InitialState, action) => {


    switch (action.type) {
		case actionTypes.GET_MOVIES:
            return {
                ...state, movies: action.payload
            }
            
        case actionTypes.OPEN_VIDEO:
            return {
                ...state, movieObj: action.payload
            }

        case actionTypes.GET_VIDEO:
            return {
                ...state
            }

        case actionTypes.ADD_HISTORY:
            return {
                ...state, history: action.payload
            }

        case actionTypes.GET_HISTORY:
            return {
                ...state.history
            }

		case actionTypes.DELETE_USER:
            return {
                ...state, users: state.users.filter(x => { return x.id !== action.payload })
			}
        default:
            break
    }
    return state
}

export default mainReducer;
