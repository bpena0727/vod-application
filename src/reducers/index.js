import { combineReducers } from 'redux';
import loggedReducer from './isLogged';
import mainReducer from './mainReducer';

export default combineReducers({
	mainReducer,
	loggedReducer,
})