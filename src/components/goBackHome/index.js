import React from "react";
import PropTypes from "prop-types";


import { Header, Icon } from 'semantic-ui-react'

class GoBackHome extends React.Component {
	static propTypes = {
		goBack: PropTypes.func,
		
	}

	render() {
		const { goBack } = this.props; 
		return(
			<Header style={{ margin: "10px !important" }} as='h2' icon textAlign='center'>
                <Header.Content>No movie selected</Header.Content>
                <h2>Go Back</h2>
                <Icon name='arrow circle left'onClick={goBack} />
            </Header>
		);
	}
}

export default GoBackHome;
