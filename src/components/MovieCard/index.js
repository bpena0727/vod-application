import React from "react";
import PropTypes from "prop-types";

import './card.css'

class MovieCard extends React.Component {
	static propTypes = {
		img: PropTypes.string,
		title: PropTypes.string,
		description: PropTypes.string,
		goToVideo: PropTypes.func,
		movieObj: PropTypes.string
	}

	render() {
		const { img, title, description, goToVideo, movieObj } = this.props; 
		return(
			<div class="movie-card" tabIndex="1">
				<div class="movie-image-container" >
					<img alt="movie-poster" class="movie-card-img" src={img} />
				</div><div>
				<div class="movie-card-container">
					<h2>{title}</h2>
					
				<div class="description-hover" data-movieobj={movieObj} onClick={goToVideo}>
					<span>{description}</span>
				</div>

				</div>
				</div>
			</div>
		);
	}
}

export default MovieCard;
