import React from "react";
import PropTypes from "prop-types";

import './style.css'


import { Card, Image, Icon } from "semantic-ui-react";

class VideoPlayerBox extends React.Component {
	static propTypes = {
		videoSrc: PropTypes.string,
	}

	componentDidMount() {
		this.videoElement.addEventListener("ended", function(){
			this.props.history.push('/videoDetails')
		});
	}

	render() {
		const { videoSrc } = this.props; 
		return(
			<div class="video-container">
				<video ref={el => this.videoElement = el} class="videoPlayer" autoPlay controls>
					<source src={videoSrc} type="video/mp4" />
					Your browser does not support the video tag.
				</video>
			</div>
		);
	}
}

export default VideoPlayerBox;
