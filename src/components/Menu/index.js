import React from 'react';
import { Menu as SemanticMenu, Icon } from 'semantic-ui-react';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './styles.css';

const MenuItem = (props) => {
    return (
        <SemanticMenu.Item>
            <Icon name={props.icon} style={{ color: "white" }} />
            <Link to={props.to}>
                {props.name}
           	</Link>
        </SemanticMenu.Item>
    );
}

class Menu extends React.Component {

	render() {
		return (
			<section>
				<header class='Navbar'>
				<nav class='Navbar-navigation'>
					<div>
						<a href='/movies' class='Navbar-logo'> 
							<FontAwesomeIcon icon="play-circle" /> 
							<h2>VOD Application</h2>
						</a>

					</div>
					<div class='spacer'>

					</div>
					<div class='Navbar-nav-items'>
						<ul>
							<li class='Navbar-nav-item'>
								<MenuItem icon="home" to="/movies" name="Home" />
							</li>
							<li class='Navbar-nav-item'>
								<MenuItem icon="save" to="/history" name="History" />
							</li>
						</ul>
					</div>
				</nav>
			</header>
				
				
						
			</section>
		);
	}

}

export default Menu;
