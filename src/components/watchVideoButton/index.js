import React from "react";
import PropTypes from "prop-types";

import './styles.css'

class WatchVideoButton extends React.Component {
	static propTypes = {
		openVideo: PropTypes.func,
		movieObj: PropTypes.string
	}

	render() {
		const { movieObj, openVideo } = this.props; 
		return(
            <button class="btn btn-raised btn-info watch-button" data-movieobj={movieObj} onClick={openVideo}>Watch Video</button>
		);
	}
}

export default WatchVideoButton;
