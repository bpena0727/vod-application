import React from "react";
import PropTypes from "prop-types";

// Lib imports
import 'semantic-ui-css/semantic.css'; //Import the css only once in your project
import { Form, Input } from 'semantic-ui-react-form-validator'
import {Button} from 'semantic-ui-react';

// was going to be used for login

class FormUser extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            id: 19,
            name: '',
            age: '', 
            role: '',
            img: ''
        };
      }


    onSubmit = (e) =>{
        let data = this.state;
        console.log(data)
    }


	render() {

		return(
            <Form ref="form" onSubmit={this.onSubmit} >
                <Input fluid type='text' label='Name' placeholder='Name' onChange={(e,{value})=>{this.setState({name:value})}} value={this.state.name} validators={['required']} errorMessages={['this field is required']} width={6} />
                <Input fluid type='number' label='Age' placeholder='Age' onChange={(e,{value})=>{this.setState({age:value})}} value={this.state.age} validators={['required']} errorMessages={['this field is required']} width={6} />
                <Input fluid label='Role' placeholder='Role' onChange={(e,{value})=>{this.setState({role:value})}} value={this.state.role} validators={['required']} errorMessages={['this field is required']} width={6} />
                <Input fluid type='file' label='Picture' placeholder='Picture' onChange={(e,{value})=>{this.setState({img:value})}} value={this.state.img} validators={['required']} errorMessages={['this field is required']} width={6} />
                <Button type='submit'>Submit</Button>
            </Form>
		);
	}
}

export default FormUser;




