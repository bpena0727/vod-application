import React from "react";
import PropTypes from "prop-types";
import { getMovies, openVideo, getVideo, addToHistory } from "../../actions/actionCreators";
import { connect } from "react-redux";
import WatchVideoButton from "../../components/watchVideoButton";
import GoBackHome from "../../components/goBackHome"
import './videoplayer.css'

// import libs
import {Grid} from 'semantic-ui-react'

class VideoDetal extends React.Component {
	static propTypes = {
        movieObj: PropTypes.object,
        openVideo: PropTypes.func,
        goBack: PropTypes.func,
        addToHistory: PropTypes.func
    }

    openVideo =  (e) =>{
		let videoOb = e.currentTarget.attributes
		for(var i=0; i<videoOb.length; i++){
			if(videoOb[i].name === "data-movieobj"){
                this.props.openVideo(JSON.parse(videoOb[i].value));
                this.props.addToHistory(JSON.parse(videoOb[i].value))
			}
		}
		
		this.props.history.push('/videoPlayer')
    }
    
    goBack =  (e) =>{
		this.props.history.push('/movies')
	}



	componentDidMount() {
        this.props.getVideo();
    }
    

	render() {
        const { movieObj } = this.props;
        if(movieObj !== "" && movieObj !== null){
            return (
                <div class="detail-container">
                    <div class="card-detail">
                	<Grid columns={12}>
                        <Grid.Row>
                            <Grid.Column width={3}>
                                <img alt="poster" src={movieObj.images[0].url}></img>
                            </Grid.Column>
                            <Grid.Column width={9}>
                                <h2>{movieObj.title}</h2>
                                {
                                    movieObj.parentalRatings && movieObj.parentalRatings.length > 0 &&
                                    movieObj.parentalRatings.map(pr => {
                                        return(	
                                            <span>Parental Rating: {pr.rating} | {movieObj.contents[0].language}</span>
                                        )
                                    })
                                }
                                
                                <br/>
                                <p class="movieDescription">{movieObj.description}</p>
                                <br/>
                                <span>Credits:
                                    {
                                        movieObj.credits && movieObj.credits.length > 0 &&
                                        movieObj.credits.map(crdts => {
                                            return(	
                                                <span> {crdts.name},</span>
                                            )
                                        })
                                    }
                                    
                                </span>
                                <br/>
                                <WatchVideoButton movieObj={JSON.stringify(movieObj)} openVideo={this.openVideo} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    </div>
                </div>
            );
        }else{
            return (
                <div class="videoplayer-container">
                    <GoBackHome goBack={this.goBack}  />    
                </div>
            );
        }
	}
}

const mapStateToProps = (state) => {
    console.log(state)
	return {
		movieObj: state.mainReducer.movieObj
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getMovies: () => {
		dispatch(getMovies())
      },
      getVideo: () => {
		dispatch(getVideo())
	  },
      openVideo: (movieObj) => {
		dispatch(openVideo(movieObj))
      }, 
      addToHistory: (movieObj) => {
        dispatch(addToHistory(movieObj))
      }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoDetal);