import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginAction} from "../../actions/login";
import { createBrowserHistory } from 'history';

// styles 
import './Login.css'


// INCOMPLETE LOING IMPLEMENTATION


class login extends React.Component {  

    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            this.props.loginAction(username, password);
            this.props.history.push('/home');
        }
    }
  
    render(){
        const { username, password, submitted } = this.state;
      return (
        <div class='img-background'>
            <div class='login-card'>
                <div className='loginForm'>
                        <h2>Login</h2>
                        <br />
                        <form name="form" onSubmit={this.handleSubmit}>
                        <div class="form-group">
                            <input type="email" name="username" value={username} onChange={this.handleChange} class="form-control" aria-describedby="emailHelp" placeholder="User" />
                            {submitted && !username &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" value={password} onChange={this.handleChange} placeholder="Password" />
                            {submitted && !password &&
                            <div className="help-block">Password is required</div>
                            }
                        </div>
                        <button class="btn btn-raised btn-info login-btn">Submit</button>
                        </form>
                </div>
            </div>
        </div>
     );
    }
  
  
}


const mapStateToProps = state => ({
    login: state.login
});

const actionCreators = {
    loginAction: loginAction,
    history: createBrowserHistory
};

const connectedLoginPage = connect(mapStateToProps, actionCreators)(login);
  
export  default withRouter (connectedLoginPage);