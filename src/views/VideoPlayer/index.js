import React from "react";
import PropTypes from "prop-types";
import { getVideo } from "../../actions/actionCreators";
import { connect } from "react-redux";
// initially had created a video box component
//import VideoPlayerBox from "../../components/VideoPlayerBox";
import GoBackHome from "../../components/goBackHome"
import './videoplayer.css'

class VideoPlayer extends React.Component {
	static propTypes = {
        movieObj: PropTypes.object,
        goBack: PropTypes.func
    }

    goBack =  (e) =>{
		this.props.history.push('/movies')
	}



	componentDidMount() {
        this.props.getVideo();      
    }
    

	render() {
        const { movieObj } = this.props;
        const myCallback = () => (this.props.history.push('/videoDetails'));

        if(movieObj !== "" && movieObj !== null){
            return (
                    <div class="video-container">
                        <video onEnded={() => myCallback()} class="videoPlayer" autoPlay controls>
                            <source src={movieObj.contents[0].url} type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                    </div>
            );
        }else{
            return (
                <div class="videoplayer-container">
                    <GoBackHome goBack={this.goBack}   />    
                </div>
            );
        }
                

        
	}
}

const mapStateToProps = (state) => {
    console.log(state)
	return {
		movieObj: state.mainReducer.movieObj
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
      getVideo: () => {
		dispatch(getVideo())
	  }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayer);