import React from "react";
import PropTypes from "prop-types";
import { getHistory, openVideo } from "../../actions/actionCreators";
import { connect } from "react-redux";
import MovieCard from "../../components/MovieCard";
import './history.css'

// import libs
import {Header, Icon } from 'semantic-ui-react'

class History extends React.Component {
	static propTypes = {
		movieHistory: PropTypes.object,
		getHistory: PropTypes.func,
	}

	goToVideo =  (e) =>{
		let videoOb = e.currentTarget.attributes
		for(var i=0; i<videoOb.length; i++){
			if(videoOb[i].name === "data-movieobj"){
				this.props.openVideo(JSON.parse(videoOb[i].value));
			}
		}
        
        
		this.props.history.push('/videoDetails')
	}
	

	componentDidMount() {
		this.props.getHistory();
	}

	render() {
        const { movies } = this.props;
        if(movies === undefined && localStorage.getItem("history") !== undefined && localStorage.getItem("history") !== null){
            const parsedResult = JSON.parse(localStorage.getItem("history"))
            const  other  = parsedResult.history
            
            return (
                <div class="scrolling-container">
                <Header style={{ margin: "10px !important" }} as='h2' icon textAlign='center'>
                    <Icon name='video' circular />
                    <Header.Content>Your History</Header.Content>
                </Header>
                        <div class="scrolling-wrapper">
                        {
                            other && other.length > 0 &&
                                other.map(movie => {
                                    return(						
                                        
                                        <MovieCard
                                            img={movie.images[0].url}
                                            title={movie.title}
                                            description={movie.description}
                                            movieObj={JSON.stringify(movie)}
                                            goToVideo={this.goToVideo}
                                        />
                                        
                                        
                                    );
                                })
                        }
                        </div>		
                </div>
            );
        }else{
            return (
                <div class="scrolling-container">
                <Header style={{ margin: "10px !important" }} as='h2' icon textAlign='center'>
                    <Icon name='video' circular />
                    <Header.Content>No movies in your history</Header.Content>
                </Header>
                </div>
            );
        }	
	}
}

const mapStateToProps = (state) => {
	return {
		movieHistory: state.mainReducer.history
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getHistory: () => {
		dispatch(getHistory())
	  },
      openVideo: (movieObj) => {
		dispatch(openVideo(movieObj))
	  }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(History);