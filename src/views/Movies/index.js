import React from "react";
import ReactDOM from 'react-dom';
import PropTypes from "prop-types";
import { getMovies, openVideo } from "../../actions/actionCreators";
import { connect } from "react-redux";
import MovieCard from "../../components/MovieCard";
import './movies.css'

// import libs
import {Header, Icon } from 'semantic-ui-react'

class Movies extends React.Component {
	static propTypes = {
		movies: PropTypes.array,
		getMovies: PropTypes.func,
	}

	goToVideo =  (e) =>{
		let videoOb = e.currentTarget.attributes
		for(var i=0; i<videoOb.length; i++){
			if(videoOb[i].name === "data-movieobj"){
				this.props.openVideo(JSON.parse(videoOb[i].value));
			}
		}
		
		this.props.history.push('/videoDetails')
	}
	

	componentDidMount() {
		this.props.getMovies();
		ReactDOM.findDOMNode(this.refs.divFocus).focus();
	}

	render() {
		const { movies } = this.props;

		return (
			<div class="scrolling-container">
			<Header style={{ margin: "10px !important" }} as='h2' icon textAlign='center'>
				<Icon name='video' circular />
				<Header.Content>Movies Available</Header.Content>
			</Header>
					<div class="scrolling-wrapper" tabIndex={0} ref="divFocus">
					{
						movies && movies.length > 0 &&
							movies.map(movie => {
								return(						
									
									<MovieCard
										img={movie.images[0].url}
										title={movie.title}
										description={movie.description}
										movieObj={JSON.stringify(movie)}
										goToVideo={this.goToVideo}
									/>
									
									
								);
							})
					}
					</div>		
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		movies: state.mainReducer.movies
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getMovies: () => {
		dispatch(getMovies())
	  },
      openVideo: (movieObj) => {
		dispatch(openVideo(movieObj))
	  }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies);